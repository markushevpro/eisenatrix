import React from 'react'
import { generate } from 'tools'

const scrolls =
    {
        renderTrackVertical:   props => <div {...props} className="v-scroll-track" />,
        renderThumbVertical:   props => <div {...props} className="v-scroll-thumb" />,
        renderTrackHorizontal: props => <div {...props} className="h-scroll-track" />,
        renderThumbHorizontal: props => <div {...props} className="h-scroll-thumb" />,
        hideTracksWhenNotNeeded: true
    }

const dates = {
    def: 'YYYY-MM-DDTHH:mm:ss',
    classic: 'DD.MM.YYYY',
    nice: 'D MMMM, YYYY',
    time: 'HH:mm'
}

const
    apiUrl = process.env.REACT_APP_BASE

const config = 
    {
        title: 'React App',

        api: {
            url: apiUrl
        },

        scrolls,

        ui: {
            prefix: '_ra',
            dates,
            stub: count => ( new Array( count || 4 ).fill( 0 ).map( ( z, i ) => ({ id: i }) ) ),
            notifications: {
                getContainer: () => document.querySelector( '.page-content' ),
                duration: 1
            },
            themes: {
                'antd': 'Светлая', 
                'verve': 'Тёмная'
            },
            themesSelect: {
                'antd': 'Включить тёмную тему',
                'verve': 'Включить светлую тему'
            },
            colorModesIndex: [
                'none',
                'tiny',
                'full',
                'adaptive'
            ],
            colorModes: {
                'none': 'Без цвета',
                'tiny': 'Легкая подсветка',
                'full': 'Полная подсветка',
                'adaptive': 'Подсветка от положения'
            },
            canShowInHead: {
                'zoom': 'Шрифт',
                'colorMode': 'Цвет',
                'theme': 'Тема'
            }
        },

        zoom: [
            '.583',
            '.75',
            '.916',
            '1',
            '1.16',
            '1.25',
            '1.5'
        ],

        def: {
            settings: {
                showInHead: [ 'theme' ],
                colorMode: 'full',
                zoom: 1,
                theme: window.matchMedia('(prefers-color-scheme:dark)').matches ? 'verve' : 'antd',
                removeImmediately: false,
                filters: [],
                keys: [ 'Не срочно', 'Срочно', 'Не важно', 'Важно' ]
            },
            events: [{
                id: generate.uniq(),
                title: 'Ваша первая задача',
                urgency: .12,
                importance: .3,
                content: 'Это ваша первая задача, введите название и описание, подберите подходящие теги (но это не обязательно).\nВ описание можно вставить ссылки, большинство из них будут адекватно работать, например:\nhttp://markushev.pro или https://nw-lab.com',
                tags: [ 'Помощь' ]
            }],
            draft: {
                _new: true,
                id: null,
                title: '',
                urgency: 0,
                importance: 0,
                content: '',
                tags: []
            }
        }
    }

export default Object.freeze( Object.assign( {}, config ) )