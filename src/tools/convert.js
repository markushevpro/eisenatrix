export const toOptions = obj =>
    {
        const
            res = []

        Object.keys( obj ).forEach( key => {
            res.push({
                key,
                title: obj[key]
            })
        })

        return res
    }

export const toOptionsByKey = ( obj, key ) =>
    {
        const
            res = []

        Object.keys( obj ).forEach( opt => {
            res.push({
                key: opt,
                title: obj[opt][key]
            })
        })

        return res
    }

export const toOptionsExtract = ( arr, keyKey, valueKey ) =>
    arr.map( item => ({
        key: item[keyKey],
        title: item[valueKey]
    }))