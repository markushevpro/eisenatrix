export const resolveAfter = ( response, time, request ) => 
    {
        return new Promise( resolve =>
            setTimeout( 
                () => {
                    console.log( 'Fake resolved:', request, response )

                    return resolve({ 
                        status: 200, 
                        data: response,
                        config: {
                            data: JSON.stringify( request )
                        }
                    })
                },
                time || 500
            )
        )
    }