export const contains = ( box, point ) =>
    {
        const
            x = point.x || point.pageX,
            y = point.y || point.pageY

        return ( x >= box.left && x <= box.left + box.width && y >= box.top && y <= box.top + box.height )
    }