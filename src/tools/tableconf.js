import * as filter from './filter'

const tableconf = precols =>
{
    const
        res = [],
        nonDefKeys = []
        
    Object.keys( precols ).forEach( key => {
        const
            cfg = precols[key],
            col = {
                ...filter.exclude( cfg, nonDefKeys ),
                key,
                dataIndex: key
            }

        ;( cfg.headSpan !== void 0 ) && (
            col.colSpan = cfg.headSpan
        )

        res.push( col )
    })

    return res
}

export default tableconf 