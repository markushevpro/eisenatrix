/* VENDOR */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Layout } from 'antd'

/* APPLICATION */
import './app-header.scss'

const
    { Header } = Layout 

class AppHeader extends Component {
    
    static propTypes = {
        title: PropTypes.string,

        onMenu: PropTypes.func
    }

    render () {
        const
            { children } = this.props,
            { onMenu } = this.props

        return (
            <Header className="app-header">
                <div className="header-menu-icon" onClick={onMenu}>
                    <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.05 11H3.95C3.42533 11 3 11.4253 3 11.95V12.05C3 12.5747 3.42533 13 3.95 13H20.05C20.5747 13 21 12.5747 21 12.05V11.95C21 11.4253 20.5747 11 20.05 11zM20.05 16H3.95C3.42533 16 3 16.4253 3 16.95V17.05C3 17.5747 3.42533 18 3.95 18H20.05C20.5747 18 21 17.5747 21 17.05V16.95C21 16.4253 20.5747 16 20.05 16zM20.05 6H3.95C3.42533 6 3 6.42533 3 6.95V7.05C3 7.57467 3.42533 8 3.95 8H20.05C20.5747 8 21 7.57467 21 7.05V6.95C21 6.42533 20.5747 6 20.05 6z" fill="#1F2022"/></svg>
                </div>
                <div className="header-logo-container">
                    <Link to="/" >
                        <img src="/medium-quality.png" alt="Medium Quality" />
                    </Link>
                </div>
                <main className="app-header-content">
                    { children }
                </main>
            </Header>
        )
    }
}

export default AppHeader
