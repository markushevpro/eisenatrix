/* VENDOR */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Input, Select } from 'antd'

/* APPLICATION */
import { generate, storage, compare } from 'tools'
import './edit-event.scss'

class EditEvent extends Component {

    static propTypes = {
        visible: PropTypes.bool,
        data: PropTypes.object,

        onSave:  PropTypes.func,
        onClose: PropTypes.func
    }

    title = React.createRef()
    text  = React.createRef()
    tags  = React.createRef()

    constructor ( props ) {
        super( props )

        this.state = {
            data: props.data ? { ...props.data } : props.data
        }

        this.set = generate.set( this )
    }

    componentDidUpdate ( prevProps ) {
        const
            { data, visible, focus } = this.props

        ;( data !== prevProps.data || ( visible !== prevProps.visible && visible ) ) && ( this.set.data({ ...data }) )
        ;( visible !== prevProps.visible && visible ) && ( setTimeout( () => this[focus || 'title'].current.focus(), 200 ) )
    }

    save = () => this.props.onSave( this.state.data )

    update = generate.update( this, 'data' )
    updateArr = generate.update( 
        this, 
        'data', 
        tags => tags.map( tag => tag.toLowerCase() ),
        () => document.querySelector( 'button' ).focus()
    )

    listTags = () =>
        {
            const
                tags = storage.extract( 'events', 'tags' )

            if ( tags.length < 1 ) {
                return <Select.Option key="none" disabled>Пока нет тегов</Select.Option>
            }

            return tags.map( tag => tag.toLowerCase() ).map( tag => (
                <Select.Option key={tag}>
                    { tag }
                </Select.Option>
            ))
        }

    content = () =>
        {
            const
                { data } = this.state

            if ( !data ) return null

            return (
                <React.Fragment>
                    <Input
                        ref = { this.title }

                        value = { data.title }
                        placeholder = "Название будет отображаться на доске"

                        onChange = { this.update( 'title', 'value' ) }
                    />
                    <Input.TextArea
                        ref = { this.text }

                        value = { data.content }
                        placeholder = "В описание удобно записать всё относящееся к задаче"
                        rows  = { 5 }

                        onChange = { this.update( 'content', 'value' ) }
                    />
                    <Select 
                        ref = { this.tags }
                        
                        mode = "tags" 
                        placeholder = "С помощью тегов можно группировать и фильтровать"
                        style = {{ width: '100%' }} 

                        value = { data.tags }

                        onChange = { this.updateArr( 'tags' ) }
                    >
                        { this.listTags() }
                    </Select>
                </React.Fragment>
            )
        }

    render () {
        const
            { visible, data } = this.props,
            changed = data && this.state.data && !compare.objects( data, this.state.data, [ '_new' ] ),
            content = this.content()

        return (
            <Modal
                title   = { data && data._new ? 'Добавить задачу' : "Редактировать задачу" }
                visible = { visible }

                okText = "Сохранить"
                okButtonProps = {{ disabled: !data || !changed || !this.state.data.title }}
                onOk = { this.save }

                cancelText = "Отмена"
                onCancel = { this.props.onClose }
            >
                { content }
            </Modal>
        )
    }
}

export default EditEvent
