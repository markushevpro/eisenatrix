/* VENDOR */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Radio, Row, Col, Input, Checkbox } from 'antd'
import { SwapOutlined } from '@ant-design/icons'

/* APPLICATION */
import { copy } from 'tools'
import config from 'config'
import './settings.scss'

class Settings extends Component {

    saveTimer = null

    static propTypes = {
        visible:  PropTypes.bool,
        settings: PropTypes.object,

        onChange: PropTypes.func,
        onClose:  PropTypes.func
    }

    fastHead = () => 
        Object.keys( config.ui.canShowInHead ).map( check => (
            <Checkbox key={check} value={check} checked={this.props.settings.showInHead.includes( check )}>
                { config.ui.canShowInHead[check] }
            </Checkbox>
        ))

    colorModes = () =>
        Object.keys( config.ui.colorModes ).map( ( mode, index ) => (
            <Radio.Button key={mode} value={mode}>
                { 
                    index === 3 || index === 0
                        ? config.ui.colorModes[mode]
                        : config.ui.colorModes[mode].split( ' ' )[0]
                }
            </Radio.Button>
        ))

    zoom = () =>
        config.zoom.map( ( z, index ) => (
            <Radio.Button key={z} value={z}>
                <span style={{ fontSize: `${z}em` }}>A</span>
            </Radio.Button>
        ))

    themes = () =>
        Object.keys( config.ui.themes ).map( theme => (
            <Radio.Button key={theme} value={theme}>
                { config.ui.themes[theme] }
            </Radio.Button>
        ))

    change = ( key, type, source ) => e => this.props.onChange( key, type, source )( e.target.value )

    swap = ( a, b ) => e =>
        {
            e.preventDefault()
            
            const
                keys = copy.array( this.props.settings.keys ),
                tmp = keys[a]

            keys[a] = keys[b]
            keys[b] = tmp

            this.props.onChange( 'keys' )( keys )
        }

    render () {
        const
            { visible, settings } = this.props

        return (
            <Modal
                title = "Настройки"
                visible = { visible }
                className = "settings-modal"

                okText = "Сохранить"
                okButtonProps = {{ style: { display: 'none' }}}

                cancelText = "Закрыть"
                cancelButtonProps = {{ style: { display: 'none' }}}
                onCancel = { this.props.onClose }
            >
                <label>Быстрый доступ из шапки</label>
                <Checkbox.Group onChange={this.props.onChange( 'showInHead' )} value={settings.showInHead}>
                    { this.fastHead() }
                </Checkbox.Group>

                <label>Подсветка</label>
                <Radio.Group onChange={this.props.onChange( 'colorMode', 'value' )} value={settings.colorMode}>
                    { this.colorModes() }
                </Radio.Group>
                
                <label>Размер шрифта</label>
                <Radio.Group onChange={this.props.onChange( 'zoom', 'value' )} value={settings.zoom}>
                    { this.zoom() }
                </Radio.Group>
                
                <label>Тема</label>
                <Radio.Group onChange={this.props.onChange( 'theme', 'value' )} value={settings.theme}>
                    { this.themes() }
                </Radio.Group>

                <label>Поля</label>
                <Row>
                    <Col span={11}>
                        <Input
                            value = { settings.keys[0] }
                            onChange = { this.change( 'keys', 'key', 0 ) }
                        />
                    </Col>
                    <Col span={2} style={{ textAlign: 'center' }}>
                        <SwapOutlined onClick={ this.swap( 0, 1 )} />
                    </Col>
                    <Col span={11}>
                        <Input
                            value = { settings.keys[1] }
                            onChange = { this.change( 'keys', 'key', 1 ) }
                        />
                    </Col>
                </Row>
                <Row>
                    <Col span={11}>
                        <Input
                            value = { settings.keys[2] }
                            onChange = { this.change( 'keys', 'key', 2 ) }
                        />
                    </Col>
                    <Col span={2} style={{ textAlign: 'center' }}>
                        <SwapOutlined onClick={ this.swap( 2, 3 )} />
                    </Col>
                    <Col span={11}>
                        <Input
                            value = { settings.keys[3] }
                            onChange = { this.change( 'keys', 'key', 3 ) }
                        />
                    </Col>
                </Row>
                <hr/>
                <small>
                    <b>Eisenatrix</b>, version 1.1.0 (2021-xx-xx)
                    <br/>
                    {/* eslint-disable-next-line */}
                    &copy; <a target="_blank" href="https://markushev.pro">markushev.pro</a>
                </small>
            </Modal>
        )
    }
}

export default Settings