/* VENDOR */
import React, { Component } from 'react'
import { Input, Row, Col, Button, Slider } from 'antd'

/* APPLICATION */
import { generate, storage } from 'tools'
import './colors.scss'

const
    sKey = 'colortest',
    saved = storage.load( sKey )

class Colors extends Component {

    constructor ( props ) {
        super( props )

        this.state = saved
            ? {
                ...saved,
                colors: null
            }
            :{
                x: 10,
                y: 10,
                rx: 10,
                ry: 10,
                r: 238,
                g: 238,
                b: 238,
                tr: 0.17,
                tg: 0.368,
                tb: 0.01,
                fr: 'r',
                fg: 'g',
                fb: 'b',
                ft: 'r',
                sfr: 'r',
                sfg: 'g',
                sfb: 'b',
                sft: 'r',
                colors: null
            }

        this.set = generate.set( this )
    }

    componentDidMount () {
        this.set.colors( this.colors() )
    }

    update = ( key, force, parse ) => e => 
        {
            ;( e.target )
                ? (
                    parse
                        ? this.set[key]( parseFloat( e.target.value ) )
                        : this.set[key]( e.target.value )
                )
                : this.set[key]( e )

            ;( force ) && ( this.apply() )
        }

    apply = () =>
        this.set.state(
            {
                sfr: this.state.fr,
                sfg: this.state.fg,
                sfb: this.state.fb,
                sft: this.state.ft,
                x: this.state.rx,
                y: this.state.ry,
            },
            () => this.set.colors(
                this.colors(),
                () => storage.save( sKey, { ...this.state, colors: null } )
            )
        )

    style = ( u, i ) =>
        {
            const
                //eslint-disable-next-line
                { x, y, r, g, b, tr, tg, tb, sfr, sfg, sfb, sft } = this.state,
                //eslint-disable-next-line
                er = Math.round( eval(sfr) ),
                //eslint-disable-next-line
                eg = Math.round( eval(sfg) ),
                //eslint-disable-next-line
                eb = Math.round( eval(sfb) ),
                //eslint-disable-next-line
                et = Math.round( eval(sft) )

            let
                //eslint-disable-next-line
                color = `rgba(${Math.round( eval(sfr) )},${Math.round( eval(sfg) )},${Math.round( eval(sfb) )})`,
                //eslint-disable-next-line
                teval = Math.round( eval(sft) ),
                text = `rgba(${teval},${teval},${teval})`

            return {
                backgroundColor: color,
                color: text,
                flex: `0 0 ${100/x}%`,
                height: `${100/y}%`
            }
        }

    colors = () =>
        {
            const
                { x, y } = this.state,
                xs = 1 / ( x - 1 ) * 2,
                ys = 1 / ( y - 1 ) * 2,
                res = []

            let
                i, u, style, c = 1

            for ( i = 1; i > -1; i = parseFloat( ( i - ys ).toFixed(5) ) ) {
                for ( u = -1; u < 1; u = parseFloat( ( u + xs ).toFixed(5) ) ) {
                    style = this.style( u, i )
                    res.push(
                        <div id={`c-${c}`} style={style} key={res.length + style.backgroundColor}>
                            Text
                        </div>
                    )
                    c++
                }
            }

            return res
        }

    render () {
        const
            { rx, ry, r, g, b, fr, fg, fb, tr, tg, tb, ft, colors } = this.state

        return (
            <div className="test-colors">
                <header>
                    <Row>
                        <Col span={1} />
                        <Col span={1}>
                            <label>X:</label>
                            <Input value = { rx } onChange={this.update( 'rx', false, true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={1}>
                            <label>Y:</label>
                            <Input value = { ry } onChange={this.update( 'ry', false, true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={1}>
                            <label>R:</label>
                            <Input value = { r } onChange={this.update( 'r', false, true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={1}>
                            <label>G:</label>
                            <Input value = { g } onChange={this.update( 'g', false, true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={1}>
                            <label>B:</label>
                            <Input value = { b } onChange={this.update( 'b', false, true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={1}>
                            <label>T R:</label>
                            <Input value = { tr } onChange={this.update( 'tr', false, true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={1}>
                            <label>T G:</label>
                            <Input value = { tg } onChange={this.update( 'tg', false, true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={1}>
                            <label>T B:</label>
                            <Input value = { tb } onChange={this.update( 'tb', false, true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={2} style={{ marginLeft: 'auto' }}>
                            <label>-</label>
                            <Button type="primary" onClick={this.apply}>Apply</Button>
                        </Col>
                        <Col span={1} />
                    </Row>
                    <Row>
                        <Col span={1} />
                        <Col span={23}>
                            <label>R Formula:</label>
                            <Input value = { fr } onChange={this.update( 'fr' )} />
                        </Col>
                        <Col span={1} />
                    </Row>
                    <Row>
                        <Col span={1} />
                        <Col span={23}>
                            <label>G Formula:</label>
                            <Input value = { fg } onChange={this.update( 'fg' )} />
                        </Col>
                        <Col span={1} />
                    </Row>
                    <Row>
                        <Col span={1} />
                        <Col span={23}>
                            <label>B Formula:</label>
                            <Input value = { fb } onChange={this.update( 'fb' )} />
                        </Col>
                        <Col span={1} />
                    </Row>
                    <Row>
                        <Col span={1} />
                        <Col span={23}>
                            <label>Text formula:</label>
                            <Input value = { ft } onChange={this.update( 'ft' )} />
                        </Col>
                        <Col span={1} />
                    </Row>
                    <Row>
                        <Col span={1} />
                        <Col span={7}>
                            <label>Red treshold:</label>
                            <Slider min={0} max={1} step={0.001} value = { tr } onChange={this.update( 'tr', true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={7}>
                            <label>Green treshold:</label>
                            <Slider min={0} max={1} step={0.001} value = { tg } onChange={this.update( 'tg', true )} />
                        </Col>
                        <Col span={1} />
                        <Col span={7}>
                            <label>Blue treshold:</label>
                            <Slider min={0} max={1} step={0.001} value = { tb } onChange={this.update( 'tb', true )} />
                        </Col>
                        <Col span={1} />
                    </Row>
                </header>
                <div className="colors-container">
                    { colors }
                </div>
            </div>
        )
    }
}

export default Colors
